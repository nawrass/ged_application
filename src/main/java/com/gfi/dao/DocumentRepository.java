package com.gfi.dao;

import com.gfi.model.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {
    Optional<Document> findById(Long id);

    List<Document> findByNomDoc(String nomDoc);

    @Query("from Document h where lower(h.nomDoc) like CONCAT('%', lower(:contains), '%')")
    public Iterable<Document> findByName(@Param("contains") String name);


}
