package com.gfi.dto.Response;

import com.gfi.model.Repertoire;

import java.util.HashSet;
import java.util.Set;

public class ImageResponseDTO extends BaseResponseDTO {

    byte[] img;

    public ImageResponseDTO(int succes, String message, byte[] img) {
        super(succes, message);
        this.img = img;
    }

    public ImageResponseDTO() {
    }

    public ImageResponseDTO(byte[] img) {
        this.img = img;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }
}
