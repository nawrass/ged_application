package com.gfi.dto.Response;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.gfi.model.Document;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeplacerDocumentResponceDTO extends BaseResponseDTO {

    private Document document;

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public DeplacerDocumentResponceDTO(int succes, String message, Document document) {
        super(succes, message);
        this.document = document;
    }

    public DeplacerDocumentResponceDTO() {
    }
}
