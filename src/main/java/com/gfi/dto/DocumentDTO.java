package com.gfi.dto;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
public class DocumentDTO {

    private Long id;

    private String nomDoc;

    private float taille;

    private String description;

    @Temporal(TemporalType.DATE)
    private Date dateCreation;

    private String type;

    public RepertoireDTO repertoire;

    public Long getId() {
        return id;
    }

    public String getNomDoc() {
        return nomDoc;
    }

    public float getTaille() {
        return taille;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public String getType() {
        return type;
    }

    public RepertoireDTO getRepertoire() {
        return repertoire;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }

    public void setTaille(float taille) {
        this.taille = taille;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRepertoire(RepertoireDTO repertoire) {
        this.repertoire = repertoire;
    }

    public DocumentDTO(Long id, String nomDoc, float taille, String description, Date dateCreation, String type, RepertoireDTO repertoire) {
        this.id = id;
        this.nomDoc = nomDoc;
        this.taille = taille;
        this.description = description;
        this.dateCreation = dateCreation;
        this.type = type;
        this.repertoire = repertoire;
    }

    public DocumentDTO() {
    }
}
