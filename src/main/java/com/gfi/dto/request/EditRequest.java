package com.gfi.dto.request;

public class EditRequest {

    private String nomDoc;


    public EditRequest(String nomDoc) {
        this.nomDoc = nomDoc;
    }

    public EditRequest() {
    }

    public String getNomDoc() {
        return nomDoc;
    }

    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }
}
