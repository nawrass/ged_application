package com.gfi.dto;

import com.gfi.model.User;

public class Question {
    private Long id;
    private String question;
    private String reponses;
    private User user;
    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getReponses() {
        return reponses;
    }

    public void setReponses(String reponses) {
        this.reponses = reponses;
    }

    public Question() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Question(Long id, String question, String reponses) {
        this.id = id;
        this.question = question;
        this.reponses = reponses;
    }
}
