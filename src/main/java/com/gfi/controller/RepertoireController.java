package com.gfi.controller;

import com.gfi.dto.RepertoireDTO;
import com.gfi.dto.Response.AllRepertoireResponseDTO;
import com.gfi.mapper.RepertoireMapper;
import com.gfi.model.Repertoire;
import com.gfi.service.Impl.RepertoireServiceImpl;
import com.gfi.service.RepertoireService;
import com.gfi.utils.ResponseMessagesUtils;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("repertoire")
public class RepertoireController {
    @Autowired
    private RepertoireServiceImpl repertoireService;
    @Autowired
    private RepertoireMapper repertoireMapper;

    @Autowired
    public RepertoireController(RepertoireService repertoireService, RepertoireMapper repertoireMapper) {
        this.repertoireService = (RepertoireServiceImpl) repertoireService;
        this.repertoireMapper = repertoireMapper;
    }

    @GetMapping(path = "/get")
    public List<Repertoire> findAll() {
        return this.repertoireService.findAll();
    }

    @GetMapping(path = "/{id_user}/create")
    public ResponseEntity<RepertoireDTO> create(@RequestParam("nomRepertoire") String nom, @PathVariable Long id_user) {

        try {
            this.repertoireService.createDirectory(nom, id_user);

            return new ResponseEntity<RepertoireDTO>(HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<RepertoireDTO>(HttpStatus.NOT_ACCEPTABLE);
        }

    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        repertoireService.DeleteDirectory(id);

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<RepertoireDTO> update(@PathVariable Long id, @RequestBody RepertoireDTO repertoireDTO) {
        Repertoire repertoire = repertoireMapper.RepertoireDTOToRepertoire(repertoireDTO);
        repertoireService.EditeDirectory(repertoire, id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(repertoireDTO);
    }


    @GetMapping(path = "/{id_user}/getAllRepertoire")
    public AllRepertoireResponseDTO getAllRepertoire(@PathVariable Long id_user) {
        AllRepertoireResponseDTO allRepertoireResponseDTO = new AllRepertoireResponseDTO();
        try {
            allRepertoireResponseDTO.setSucces(1);
            allRepertoireResponseDTO.setMessage(ResponseMessagesUtils.ALLREPERTOIRE_SUCCESS_MESSAGE);
            allRepertoireResponseDTO.setRepertoires(repertoireService.getAllRepertoire(id_user));
            return allRepertoireResponseDTO;
        } catch (Exception e) {
            allRepertoireResponseDTO.setSucces(0);
            allRepertoireResponseDTO.setMessage(e.getMessage());
            return allRepertoireResponseDTO;
        }
    }

}
