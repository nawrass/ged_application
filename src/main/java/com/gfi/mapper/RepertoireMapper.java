package com.gfi.mapper;

import com.gfi.dto.DocumentDTO;
import com.gfi.dto.RepertoireDTO;
import com.gfi.model.Repertoire;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RepertoireMapper {

    //RepertoireMapper INSTANCE = Mappers.getMapper( RepertoireMapper.class );

    RepertoireDTO repertoireDTO(Repertoire repertoire);

    List<RepertoireDTO> toRepertoireDTOs(List<Repertoire> repertoire);

    Repertoire RepertoireDTOToRepertoire(RepertoireDTO dto);

}
