package com.gfi.service.Impl;

import com.gfi.constant.ApplicationConfig;
import com.gfi.dao.DocumentRepository;
import com.gfi.dao.RepertoireRepository;
import com.gfi.dao.UserRepository;
import com.gfi.dto.RepertoireDTO;
import com.gfi.mapper.RepertoireMapper;
import com.gfi.model.Document;
import com.gfi.model.Repertoire;
import com.gfi.model.User;
import com.gfi.service.RepertoireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
//import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class RepertoireServiceImpl implements RepertoireService {
    @Autowired
    RepertoireRepository repertoireRepository;

    @Autowired
    private RepertoireMapper repertoireMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    DocumentRepository documentRepository;


    public RepertoireServiceImpl(RepertoireMapper repertoireMapper, RepertoireRepository repertoireRepository) {
        this.repertoireRepository = repertoireRepository;
        this.repertoireMapper = repertoireMapper;
    }

    public boolean createDirectory(String nom, Long id_user) throws IOException, ParseException {
        Repertoire repertoire = new Repertoire();
        Optional<User> user = this.userRepository.findById(id_user);
        repertoire.setNomRepertoire(nom);
        List<Long> ids = getRepertoireids();
        ids.sort(Comparator.reverseOrder());
        repertoire.setCodeRepertoire(repertoire.formatCodeRepertoire(ids.get(0) + 1));
        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        repertoire.setDateCreation(new SimpleDateFormat("yyyy-MM-dd").parse(date));
        repertoire.setUser(user.get());
        File directory = new File(ApplicationConfig.TEMP_DIR, repertoire.getCodeRepertoire());
        this.repertoireRepository.save(repertoire);

        if (!directory.exists()) {
            if (directory.mkdir()) {
                return true;
            }
        }
        return false;

    }

    @Override
    public List<Long> getRepertoireids() {

        return repertoireRepository.getAllIds();
    }

    @Override
    public Set<Repertoire> getAllRepertoire(Long id_user) throws Exception {
        return this.userRepository.findById(id_user).get().getRepertoires();


    }

    @Override
    public List<Repertoire> searchByNameDoc(String nomDoc, Long id_user) throws Exception {
        List<Repertoire> repertoires = new ArrayList<>();
        List<Repertoire> uniqueList = new ArrayList<>();
        User users = this.userRepository.findById(id_user).get();
        Iterable<Document> documents = this.documentRepository.findByName(nomDoc);

        List<Document> parcour = new ArrayList<>();
        documents.forEach(parcour::add);

        for (Document document : parcour) {
            //while (documents.iterator().hasNext()){

            Repertoire repertoire = new Repertoire();
            repertoire.setId(document.getRepertoire().getId());
            repertoire.setNomRepertoire(document.getRepertoire().getNomRepertoire());
            repertoire.setCodeRepertoire(document.getRepertoire().getCodeRepertoire());
            repertoire.setDateCreation(document.getRepertoire().getDateCreation());
            repertoire.setUser(document.getRepertoire().getUser());
            repertoire.setDocuments(new HashSet<>());

            //  repertoire.getDocuments().add(documents.iterator().next());
            if (users.equals(repertoire.getUser())) {
                if ((!repertoires.contains(repertoire))) {
                    repertoires.add(repertoire);
                }

                int i = repertoires.indexOf(repertoire);
                Repertoire repertoire1 = repertoires.get(repertoires.indexOf(repertoire));
                repertoires.get(repertoires.indexOf(repertoire)).getDocuments().add(document);
            }
        }
       /* uniqueList = new ArrayList<>(repertoires);
        for (Document document:parcour){
            Repertoire rep1=uniqueList.get(0);
            int index=uniqueList.indexOf(rep1);
            System.out.println(rep1+" ** indice"+ uniqueList.indexOf(rep1)+"");
            //System.out.println(uniqueList.get(uniqueList.indexOf(f.next().getRepertoire())));
            uniqueList.get(uniqueList.indexOf(document.getRepertoire())).getDocuments().add(document);

        }*/


        return repertoires;
    }


    @Override
    public Optional<Repertoire> findByID(Long id) {
        return this.repertoireRepository.findById(id);
    }

    @Override
    public boolean EditeDirectory(Repertoire Repertoire, Long idRep) {
        this.repertoireRepository.findById(idRep);
        Set<Document> document = new HashSet<>();

        Repertoire.setId(idRep);
        Repertoire.setDocuments(document);

        repertoireRepository.save(Repertoire);
        return true;
    }

    @Override
    public void DeleteDirectory(Long id) {
        Optional<Repertoire> repertoire = repertoireRepository.findById(id);

        if (repertoire.isPresent()) {
            this.repertoireRepository.deleteById(id);
        }
    }

    @Override
    public List<Repertoire> findAll() {
        return this.repertoireRepository.findAll();

    }


}

