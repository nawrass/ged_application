package com.gfi.service.Impl;

import com.gfi.constant.CustomException;
import com.gfi.dao.RoleRepository;
import com.gfi.dao.UserRepository;
import com.gfi.model.Role;
import com.gfi.model.RoleName;
import com.gfi.model.User;

import com.gfi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository utilisateurRepository;

    @Autowired
    private RoleRepository roleRepository;



    public String signin(String email, String password) {
//        try {
//            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
//            return jwtTokenProvider.createToken(email, utilisateurRepository.findByEmail(email).get().getRoles());
//        } catch (AuthenticationException e) {
//            throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
//        }
        return null;
    }

    public String signup(User user) {

        return null;
    }


    @Override
    public List<User> getAllusers() {
        List<User> clients = new ArrayList<>();
        this.utilisateurRepository.findAll().forEach(item -> {
            if (item.getRoles().contains(RoleName.ROLE_USER) && !item.getRoles().contains(RoleName.ROLE_ADMIN)) {
                clients.add(item);
            }
        });
        return clients;
    }


    @Override
    public boolean saveUser(User utilisateur) {
        return false;
    }

    @Override
    public boolean deleteUser(User utilisateur) {
        return false;
    }

    @Override
    public Optional<List<User>> findByUsername(String username) {
        return Optional.empty();
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return this.utilisateurRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findById(Long id) {
        return this.utilisateurRepository.findById(id);
    }

    @Override
    public boolean updateUsername(String email, String username) {
        Optional<User> opt = this.utilisateurRepository.findByEmail(email);
        User user;
        if (opt.isPresent()) {
            user = opt.get();
            user.setUsername(username);
            this.utilisateurRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public boolean updatePassword(String username, String oldPass, String newPass) {

        return false;
    }

    @Override
    public boolean updateImageName(String email, String imgName) {
        Optional<User> opt = this.utilisateurRepository.findByEmail(email);
        User user;
        if (opt.isPresent()) {
            user = opt.get();
            user.setImageName(imgName);
            this.utilisateurRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public User findByEmailAndPassword(String email, String password) {
        return null;
    }

    @Override
    public int getNubmerBookDoneOfUser(String email) {
        return 0;
    }


    @Override
    public String getUserImage(Long id) {
        Optional<User> opt = this.utilisateurRepository.findById(id);
        User user;
        if (opt.isPresent()) {
            user = opt.get();

            return user.getImageName();
        }
        return "";
    }
}
