package com.gfi.constant;

import com.google.common.io.Files;

import java.io.File;
import java.util.Optional;

public class FileUtil {
    public static Optional<String> getExtensionByName(String name) {
        return Optional.ofNullable(name)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(name.lastIndexOf(".") + 1));

    }

    public String getExtensionByURl(String URl) {
        return Files.getFileExtension(URl);
    }
}
